/*
Nicholas Sam and Sammy Parham
2018-11-29
Naccarato
TEJ4M

This game is a pong clone using
an arduino and an 8*8 led matrix display
*/

//setup pins
int latchPin = 4;
int clockPin = 5;
int dataPin = 3;
int dataPin2 = 6;
int latchpin2 =7;
int up = 13;
int down = 12;

//setup varibles
byte DATA[8];
const int TWO[8] = {1, 2, 4, 8, 16, 32, 64, 128};
int ballx =3;
int bally =3;
int p1location = 3;
int p2location = 3;
int y=1;
int x=1;
int move;
//int mb;
int shifty;
int p;

void setup()
{
  //start serial
  //Serial.begin(9600);
  //setup pins
  pinMode(latchPin, OUTPUT);
  pinMode(latchpin2, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(dataPin2, OUTPUT);
  pinMode(up, INPUT);
  pinMode(down, INPUT);
  //turn on pins for paddles
  set(7, p1location+1, 1);
  set(7, p1location, 1);
  set(0, p2location+1, 1);
  set(0, p2location, 1);
}

void loop()
{
  //move player
  pMove();
  //move ai only if ball is in range so that ai dosnt win every time
  if(ballx<=2)
  {
    ai();
  }
  //move ball
  moveBall();
  //draw everything
  display();
  //check for winner
  checkwin();
}

void pMove()
{
  //prevents player from moving ever loop / too quickly
  if(p>=20)
  {
    //check if should move up or down
    if(digitalRead(up) && p1location!=6)
    {
      //move up
      set(7,p1location,0);
      p1location++;
      set(7, p1location+1, 1);
    }
    if(digitalRead(down) && p1location!=0)
    {
      //move down
      set(7, p1location+1, 0);
      p1location--;
      set(7, p1location, 1);
    }
    //reset p so player dosnt move next loop
    p=0;
  }
  //increase p every loop
  p++;
}

void ai()
{
  if(move>=40)
  {
    //find where ball is and move he ycord of ai to match, very similar to player movment
    //but based on ball location instead of user input
    if(p2location<bally || (p2location-1)<bally&& p2location!=6)
    {
      set(0,p2location,0);
      p2location++;
      set(0,p2location+1, 1);
    }
    if(p2location>bally || (p2location+1)>bally && p2location!=0)
    {
      set(0, p2location+1, 0);
      p2location--;
      set(0, p2location, 1);
    }
    //delay(200);
  }
}
void moveBall()
{
  //prevents ball from moving to fast by not moving every loop
  if(move >=40)
  {
    //reset move to 0 so it does not move for a bit
    move=0;
    //check if ball is hitting an edge or a paddle and change x and y velocities
    if(ballx==6 && bally==p1location)
    {
      x =-1;
      y=-1;
    }
    if(ballx==6 && bally==p1location+1)
    {
      x =-1;
      y= 1;
    }
    if(ballx==1 && bally==p2location)
    {
      x =1;
      y=-1;
    }
    if(ballx==1 && bally==p2location+1)
    {
      x =1;
      y= 1;
    }
    if(bally==7)
    {
      y=-1;
    }
    if (bally == 0)
    {
      y=1;
    }
    //remove old ball
    set(ballx,bally,0);
    //change ball location based on velocitys
    ballx+=x;
    bally+=y;
    //add new ball
    set(ballx,bally,1);
  }
  //increase move for next time
  move++;
}

void set(int x, int y, byte value)
{
  //cehck if pixel should turn on
  if (value == 1)
  {
    //turn on corasponding byte
    DATA[x] = DATA[x]|TWO[y];
  }
  //check if pixle should turn off
  else if (value == 0)
  {
    //turn off corasponding byte
    DATA[x] = DATA[x]&~TWO[y];
  }
}

void display()
{
  //loop once for each row
  for(int i = 0; i<=7; i++)
  {
    //get the data and shift it out
    shifty = DATA[i];
    //only shift if there is soemthing to display to prevent watsing time leaving off pixels off
    if(shifty!=0)
    {
      //small delay to prevent ghosting but adds a bit of flickering
      delay(07);
      //shift to each register
      shift(~shifty, dataPin, latchPin);
      shift(TWO[i], dataPin2, latchpin2);
    }
  }
}

void shift(byte data, int pin, int latch)
{
  //write to the shift register and then output
  digitalWrite(latch, LOW);
  shiftOut(pin, clockPin, MSBFIRST, data);
  digitalWrite(latch, HIGH);
}
void checkwin()
{
  //check if ball is "out of bounds" then serial print who won and reset
  if(ballx>=7)
  {
    clear();
    delay(20);
    //Serial.print("p2 win");
    for(int i=0; i<=7; i++)
    {
      set (0,i,1);
    }
    for(int i = 0; i<=200; i++)
    {
      display();
    }
    clear();
    reset();
  }
  if(ballx<=0)
  {
    clear();
    delay(20);
    //Serial.print("p1 win");
    for(int i=0; i<=7; i++)
    {
      set (7,i,1);
    }
    for(int i = 0; i<=200; i++)
    {
      display();
    }
    clear();
    reset();
  }
}

void clear()
{
  //delay
  delay(20);
  //reset all pixles to off
  for(int i=0; i<=7; i++)
  {
    for(int j=0; j<=7; j++)
    {
      set(i,j,0);
    }
  }
  display();
  display();
}
void reset()
{
  //reset varibles
  ballx =3;
  bally =3;
  p1location = 3;
  p2location = 3;
  x=1;
  y=1;
  //redraw importnat stuff (ball will redraw next loop)
  set(7, p1location+1, 1);
  set(7, p1location, 1);
  set(0, p2location+1, 1);
  set(0, p2location, 1);
  display();
}
