/*
  Shift Register Example
  Turning on the outputs of a 74HC595 using an array

 Hardware:
 * 74HC595 shift register
 * LEDs attached to each of the outputs of the shift register

 */
//Pin connected to ST_CP of 74HC595
int latchPin = 4;
int latchpin2 =7;
//Pin connected to SH_CP of 74HC595
int clockPin = 5;
////Pin connected to DS of 74HC595
int dataPin = 3;
int dataPin2 = 6;
int array[8] = {0,1,1,1,1,1,1,0};

int b;

//holders for infromation you're going to pass to shifting function

void setup()
{
  Serial.begin(9600);
  //set pins to output because they are addressed in the main loop
  pinMode(latchPin, OUTPUT);
  //Serial.begin(9600);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
  pinMode(dataPin2, OUTPUT);
  pinMode(latchpin2, OUTPUT);
  //String b = "0B" + "01111110";
  //B.concat("01111110");
  //c = b.toInt;
  //Serial.print(B);
}

void loop()
{
  /*for (int i = 0; i < 256; i++)
  {
    delay(1000);
    shift(~i, dataPin);
    delay(1000);
    shift(i, dataPin2);
  }*/
  //shift(~56, dataPin, latchPin);
  //shift(57,dataPin2, latchpin2);
  //b = array[7]*pow(2,0) +array[6]*pow(2,1)+array[5]*pow(2,2)+array[4]*pow(2,3)+array[3]*pow(2,4)+array[2]*pow(2,5)+array[1]*pow(2,6)+array[0]*pow(2,7);

  /*digitalWrite(latchPin, LOW);
  shiftOut(dataPin, clockPin, MSBFIRST, 0B01011110);
  digitalWrite(latchPin, HIGH);*/

  shift(~4, dataPin, latchPin); //y cord
  shift(2, dataPin2, latchpin2); //x cord

  //digitalWrite(latchpin2, LOW);
  //shiftOut(dataPin2, clockPin, MSBFIRST, 0B10000001);
  //digitalWrite(latchpin2, HIGH);
}

void shift(byte data, int pin, int latch)
{
  digitalWrite(latch, LOW);
  shiftOut(pin, clockPin, MSBFIRST, data);
  digitalWrite(latch, HIGH);
}
